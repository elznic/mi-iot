# Automatická kontrola žaluzií

## Popis

Práce řeší sestrojení sestavy a následný vývoj systému, který automaticky
roztahuje a zatahuje žaluzie. To se děje na základě uživatelem definovaného
času pro roztažení a zatažení. Dále pak systém umožňuje snadno přidávat nové
žaluzie, které mohou být ovládány.

## Využívané technologie

* NodeJS
* MongoDB
* RPi3
* 
