var Blind = require('../models/blind');
var Gpio = require('pigpio').Gpio;

const UP = 2200;
const DOWN = 1300;
const ARET = 100;

setInterval(function () {

    Blind.find().exec(function(err,blinds){
        if(err)
            return console.log(err);
        var date = new Date();
        console.log("Checking "+date);
        blinds.forEach(function(blind){
            if(date.getHours() == Math.floor(blind.morning /60) && date.getMinutes() === blind.morning%60){
                console.log("It's time to go up...\n");
                go(blind,UP);
            }
            if(date.getHours() == Math.floor(blind.evening /60) && date.getMinutes() === blind.evening%60){
                console.log("It's time to go down...\n");
                go(blind,DOWN);
            }
        });
    });
}, 60*1000);

exports.view= function(req, res) {
    Blind.find().exec(function(err,blinds){
        if(err)
            return console.log(err);
        res.render('blind_view', {title: 'Kontrola žaluzií', blinds: blinds});
    });
}

exports.detail = function(req, res) {
    Blind.find({ name: req.params.blindName }).exec(function(err,blinds){
        if(err)
            return console.log(err);
        res.render('blind_view', {title: 'Kontrola žaluzií', blinds: blinds});
    });
};

exports.delete = function(req, res) {
    Blind.remove({ name: req.params.blindName }).exec(function(err,blinds){
        if(err)
            return console.log(err);
        res.redirect('/')
    });
};

exports.edit_form = function(req, res) {
    Blind.find({ name: req.params.blindName }).exec(function(err,blinds){
        if(err)
            return console.log(err);
        res.render('blind_form', {title: 'Kontrola žaluzií', blind: blinds[0]});
    });
};

exports.edit = function (req,res) {
    var array = req.body.morning.split(":");
    var morning = parseInt(array[0])*60 + parseInt(array[1]);
    array = req.body.evening.split(":");
    var evening = parseInt(array[0])*60 + parseInt(array[1]);
    Blind.find({ name: req.body.name }).exec(function(err,blinds){
        var blind;
        if(blinds.length > 0){
            blind = blinds[0];
            blind.set({
                pin: req.body.pin,
                name: req.body.name,
            });
        }else{
            blind = new Blind({
                pin: req.body.pin,
                name: req.body.name,
            });
        }
        blind.set({morning: morning});
        blind.set({evening: evening});
        blind.save(function (err) {
            if (err) {
                console.log(err);
                res.render('blind_form', {blind: blind, error: err});
                return;
            }
            res.redirect('/blind/'+blind.name);
        });
    });
};


exports.switch = function(req, res) {
    Blind.find({ name: req.params.blindName }).exec(function(err,blinds){
        if(err)
            return console.log(err);
        var blind = blinds[0];
        if(blind !== undefined && blind.position === UP){
            goDown(blind);
        }
        else{
            goUp(blind);
        }
        res.redirect(req.headers.referer);
    });

};

var goUp = function(blind){
    go(blind, UP+ARET);
    setTimeout(function (blind) {
        go(blind, UP);
    }, 1000, blind);
};

var goDown = function(blind){
    go(blind, DOWN-ARET);
    setTimeout(function (blind) {
        go(blind, DOWN);
    }, 1000, blind);
};

var go = function (blind,dest) {
    var motor = new Gpio(blind.pin, {mode: Gpio.OUTPUT});
    blind.position = dest;
    motor.servoWrite(dest);
    blind.save(function (err) {
        if (err) {
            return next(err);
        }
    });
};
