var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BlindSchema = new Schema(
    {
        pin: { type: Number, unique : true, required : true },
        name: { type: String, unique : true, required : true },
        morning: { type: Number },
        evening: { type: Number },
        position: { type: Number },
    }
);

// Virtual for this author instance URL.
BlindSchema
    .virtual('url')
    .get(function () {
        return '/blind/'+this._id
    });

BlindSchema
    .virtual('name-url')
    .get(function () {
        return '/blind/'+this.name
    });

BlindSchema
    .virtual('morningFormat')
    .get(function () {
        return ("0" + Math.floor(this.morning/60)).slice(-2)+":"+("0" + (this.morning%60)).slice(-2);
    });

BlindSchema
    .virtual('eveningFormat')
    .get(function () {
        return ("0" + Math.floor(this.evening/60)).slice(-2)+":"+("0" + (this.evening%60)).slice(-2);
    });

// Export model.
module.exports = mongoose.model('Blind', BlindSchema);
