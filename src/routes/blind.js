var express = require('express');
var router = express.Router();

var blind_controller = require('../controllers/blindController');

router.get('/', blind_controller.view);
//router.get('/blind/', blind_controller.view);
router.post('/blind/', blind_controller.edit);
router.get('/blind/:blindName', blind_controller.detail);
router.post('/blind/:blindName', blind_controller.edit);
router.get('/blind/:blindName/edit', blind_controller.edit_form);
router.get('/blind/:blindName/delete', blind_controller.delete);



router.get('/switch/:blindName', blind_controller.switch);
router.get('/dev/delete', function (req,res) {
    var Blind = require('../models/blind');
    Blind.collection.drop();
    res.redirect('/');
})
module.exports = router;